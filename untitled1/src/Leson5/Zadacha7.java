package Leson5;

public class Zadacha7 {
    public static void main(String[] args) {
        double max = -10;
        double y = 0;
        for (double x = -10; x <= 10; x += 0.01) {
            if (1 / Math.pow(x, 3) + Math.pow(x, 3) > 1 / Math.pow(max, 3) + Math.pow(x, 3)) {
                if (x == 0.0) continue;
                max = x;
                y = 1 / Math.pow(max, 3) + Math.pow(x, 3);

            }

        }
        System.out.println("Значение y=" + y + " Значение x=" + max);
    }
}
