import java.util.Random;

public class Primeri {
    public static void main(String[] args) {
        Random random = new Random();//через него рандомят цифры
        double[] data = new double[5];//массив с датой

        //заполнение массива случайными значениями
        for (int i = 0; i < data.length; i++) {
            data[i] = random.nextInt(99) + 1;
        }

        //печать массива на экран
        for (int i = 0; i < data.length; i++) {
            System.out.println(data[i]);
        }

        //нахождение максимума
        double max = data[0];//переменная для хранения max
        for (int i = 0; i < data.length; i++) {
            if (data[i] > max) {
                max = data[i];
            }
        }

        System.out.println("Максимум - " + max);

        //нормаирование относительно максимума
        for (int i = 0; i < data.length; i++) {
            data[i] = data[i] / max;
        }

        //печать массива на экран
        for (int i = 0; i < data.length; i++) {
            System.out.println(data[i]);
        }
//        int[][] data = new int[5][5];
//
//        fillData(data,50);
//        printArray(data);
//    static void printArray(int[][] array){
//        for (int i = 0; i < array.length; i++) {
//            for (int j = 0; j <array[i].length ; j++) {
//                System.out.print(array[i][j] + " ");
//            }
//            System.out.println();
//        }
//    }
//
//    static void fillData(int[][] array, int top){
//        Random random = new Random();
//        for (int i = 0; i < array.length; i++) {
//            for (int j = 0; j <array[i].length ; j++) {
//                array[i][j] = random.nextInt(top);
//            }
//        }

    }
}
